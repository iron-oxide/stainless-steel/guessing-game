# Guessing Game

A slightly longer hello world that introduces:

- IO
- types
- pattern matching
- using external libraries (crates)
- loops
- handling invalid input
- variables

To play run:

    cargo run
